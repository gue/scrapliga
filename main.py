from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from scrapliga.spiders.oehb import OeHBSpider

process = CrawlerProcess(get_project_settings())

process.crawl(OeHBSpider)
process.start()

