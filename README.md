
# scrapliga

[Scrapy](https://scrapy.org/) Spider to crawl through https://oehb-handball.liga.nu/.


https://codeberg.org/gue/scrapliga

## Status

This project is in a experimental state.


## Scraped Data

Following items are being scraped:

* Championship
* ChampionshipGroup
* Club
* Team
* Court
* Match
* Player (Not yet implemented)

See [scrapliga.items](https://codeberg.org/gue/scrapliga/src/branch/main/scrapliga/items.py) for details.  
Caveat: Information on https://oehb-handball.liga.nu/ is quite complicated presented, resulting in a rather
unintuitive item structure.


## Execute

Crawl site (currently only WHV parts are processed) and store
results in directory `feeds` as JSON files.

    [ -d feeds ] && rm feeds/*
    python3 main.py
    ls feeds/


## Development Setup

Initialize development environment via Pipenv.

Run scrapy interactively to explore nuliga HTML responses:

    pipenv shell
    scrapy shell
    >>> fetch('https://oehb-handball.liga.nu/cgi-bin/WebObjects/<some path here>...')
    >>> response.xpath("//h1/text()").getall()
    ['...','....']
    >>> response.xpathc("<your query")