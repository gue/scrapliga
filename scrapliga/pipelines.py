# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem
from scrapliga import items
import logging


class ScrapligaPipeline:

    championships = []
    groups = []
    clubs = []
    teams = []
    matches = []
    courts = []

    def __init__(self):
        self.logger = logging.getLogger(ScrapligaPipeline.__name__)

    def process_item(self, item, spider):
        if isinstance(item, items.Club):
            return self.process_club(item)
        if isinstance(item, items.Championship):
            return self.process_championship(item)
        if isinstance(item, items.ChampionshipGroup):
            return self.process_championship_group(item)
        if isinstance(item, items.Team):
            return self.process_team(item)
        if isinstance(item, items.Match):
            return self.process_match(item)
        if isinstance(item, items.Court):
            return self.process_court(item)

    def process_club(self, item: items.Club):
        if item.get('name') in ["Wiener Handball Verband"]:
            # Pseudo / Invalid club. Ignore.
            return
        if item not in self.clubs:
            self.clubs.append(item)
            return item

    def process_championship(self, item: items.Championship):
        if item not in self.championships:
            self.championships.append(item)
            return item

    def process_championship_group(self, item: items.ChampionshipGroup):
        if item not in self.groups:
            self.groups.append(item)
            return item

    def process_team(self, item: items.Team):
        if item not in self.teams:
            self.teams.append(item)
            return item

    def process_match(self, item: items.Match):
        if item not in self.matches:
            self.matches.append(item)
            return item

    def process_court(self, item: items.Court):
        if item not in self.courts:
            self.courts.append(item)
            return item
