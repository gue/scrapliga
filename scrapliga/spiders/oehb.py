from datetime import datetime
import scrapy
import re
from urllib.parse import urlparse, parse_qs, urlsplit
from scrapliga import items
from scrapliga.spiders.parse_utils import parse_str_from_xpath_text, parse_int_from_xpath_text

def get_url_query_param(url: str, param: str) -> str|None:
    url_components = urlparse(url)
    url_query = parse_qs(url_components.query)
    if param not in url_query:
        return None
    return url_query.get(param)[0]


def get_club_id_from_url(url: str) -> str|None:
    """
    example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/clubInfoDisplay?club=89
    :param url:
    :return:
    """
    return get_url_query_param(url, 'club')


def get_association_short_name_from_url(url: str):
    """
    example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/groupPage?targetFed=%C3%96HB&championship=%C3%96HB+22%2F23&group=211633
    :param url:
    :return:
    """
    return get_url_query_param(url, 'targetFed')


def get_group_id_from_url(url: str):
    """
    example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/groupPage?targetFed=%C3%96HB&championship=%C3%96HB+22%2F23&group=211633
    :param url:
    :return:
    """
    return get_url_query_param(url, 'group')


def get_championship_short_name_from_url(url: str):
    return get_url_query_param(url, 'championship')


def get_team_id_from_url(url: str):
    return get_url_query_param(url, 'teamtable')


def get_court_id_from_url(url: str):
    return get_url_query_param(url, 'location')


def get_match_id_from_url(url: str):
    return get_url_query_param(url, 'meeting')

def get_player_id_from_url(url: str) -> str|None:
    """
    example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/playerPortrait?season=2024%2F25&federation=%C3%96HB&person=2475370&targetFed=%C3%96HB
    :param url:
    :return:
    """
    return get_url_query_param(url, 'person')


class OeHBSpider(scrapy.Spider):
    name = 'oehb'
    allowed_domains = ['oehb-handball.liga.nu']
    start_urls = [
        'https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/clubSearch?searchPattern=AT.01&federation=WHV&regionName=Wien&federations=WHV'
    ]

    def parse(self, response: scrapy.http.Response, **kwargs):
        return self.parse_club_search_page(response)

    def parse_club_search_page(self, response: scrapy.http.Response):
        rows = response.xpath("//table[has-class('result-set')]/tr")
        for row in rows:
            if row.xpath("./th"):
                # table header
                continue
            # validate row by checking club name
            # expected cell value: "some club (123432)"
            name_cell = re.sub(" +", " ", " ".join(row.xpath("./td[1]//text()").getall()).replace("\n", "")).strip()
            if not re.match("^.+ \\(\d+\\)$", name_cell):
                self.logger.debug("parse_club_search_page: invalid club listing -- %s" % name_cell)
                continue
            club_info_link = row.xpath("./td[1]/a")
            club_info_url = response.urljoin(club_info_link.attrib.get('href'))
            yield scrapy.Request(club_info_url, callback=self.parse_club_info)

    def parse_club_info(self, response: scrapy.http.Response):
        """
        Example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/clubInfoDisplay?club=106
        """
        club_id = get_club_id_from_url(response.url)
        title = response.xpath("//h1/text()").getall()
        # expected: first line is association, second line is club
        if len(title) < 2:
            # unexpected title format
            self.logger.warn("unexpected <h1> format in %s" % response.url)
            return
        association_name = title[0].strip()
        club_name = title[1].strip()
        web_url = response.xpath("//h2[text()='Kontaktadresse']/following-sibling::p/a[@target='_blank']/@href").get()
        if web_url:
            web_url = web_url.replace("http://", "https://")
        club = items.Club(nuliga_id=club_id, name=club_name, website=web_url)
        yield club
        club_teams_url = response.xpath("//a[text()=\"Mannschaften und Ligeneinteilung\"]").attrib.get('href')
        yield scrapy.Request(response.urljoin(club_teams_url), callback=self.parse_club_teams_page, cb_kwargs={'club': club})

        court_links = response.xpath("//b[contains(.,'Hallen')]//following::ul/li/a")
        for court_link in court_links:
            court_info_page_url = response.urljoin(court_link.xpath("./@href").get())
            yield scrapy.Request(court_info_page_url, callback=self.parse_court_info_page)

    def parse_club_teams_page(self, response: scrapy.http.Response, club: items.Club):
        """
        Parse `clubTeams` page.
        Example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/clubTeams?club=91
        :param response:
        :param club:
        """
        rows = response.xpath("//table[has-class('result-set')]/tr")
        current_championship = None
        current_association_short_name = None
        for row in rows:
            if row.xpath("./td[@colspan=5]"):
                # championship row
                current_championship = row.xpath("./td/h2/text()").get()
                current_association_short_name = None
                continue

            if row.xpath("./th"):
                # only column headers
                continue

            team_name = row.xpath("./td[1]/text()").get()
            group_page_link = row.xpath("./td[2]/a")
            group_name = group_page_link.xpath("string(.)").get()
            group_page_url = response.urljoin(group_page_link.attrib.get('href'))
            if not current_association_short_name:
                current_association_short_name = get_association_short_name_from_url(group_page_url)
            yield scrapy.Request(group_page_url, callback=self.parse_group_page)

    def parse_group_page(self, response: scrapy.http.Response):
        """
        Example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/groupPage?championship=WHV+22%2F23&group=211869
        :param response:
        :return:
        """
        title = response.xpath("//h1/text()").getall()
        # expected: first line is championship, second line is group name, third is "Tabelle und Spielplan (Aktuell)"
        if len(title) < 3:
            # unexpected title format
            self.logger.warn("unexpected <h1> format in %s" % response.url)
            return

        championship_name = title[0].strip()
        group_name = title[1].strip()

        sections = response.xpath("//h2/text()").getall()
        if "Tabelle" not in sections:
            # skip leagues without standings
            self.logger.debug("Skipping group %s since no standings table found - %s" % (group_name, response.url))
            return

        group_id = get_group_id_from_url(response.url)
        group_report_pdf = response.urljoin(response.xpath("//a[@class='picto-pdf']").attrib.get('href'))

        # read championship short name from some arbitrary link
        some_group_url = response.urljoin(response.xpath("//a[contains(.,'Spielleiter und Schiedsrichter-Einteiler')]/@href").get())
        championship_short_name = get_championship_short_name_from_url(some_group_url)

        yield items.Championship(nuliga_id=championship_short_name, name=championship_name,
                                 short_name=championship_short_name)

        tables = response.xpath("//table[has-class('result-set')]")
        # parse group info
        for table in tables:
            # not yet implemented
            pass

        group_team_ids = []
        group_standing = []
        # iterate ranking table
        for table in tables:
            if "Rang" != table.xpath(".//th[2]/text()").get():
                # not ranking table
                continue
            rows = table.xpath(".//tr")
            for row in rows:
                if row.xpath("./th"):
                    # skip header row
                    continue
                rank = int(row.xpath("./td[2]/text()").get())
                team_link = row.xpath("./td/a")
                team_display_name = team_link.xpath("string(.)").get().strip()
                if parse_int_from_xpath_text(row, "./td[5]") is not None:
                    match_count = parse_int_from_xpath_text(row, "./td[4]")
                    if match_count > 0:
                        win_count = parse_int_from_xpath_text(row, "./td[5]")
                        draw_count = parse_int_from_xpath_text(row, "./td[6]")
                        lost_count = parse_int_from_xpath_text(row, "./td[7]")
                        td8 = parse_str_from_xpath_text(row, "./td[8]")
                        try:
                            (goals_for, goals_against) = td8.split(":", 2) if td8 else (None, None)
                            goals_for = int(goals_for) if goals_for else None,
                            goals_against = int(goals_against) if goals_against else None,
                        except ValueError as e:
                            (goals_for, goals_against) = (None, None)
                            self.logger.warn("Standings table has invalid goals stats \"%s\" in URL %s " % (td8, response.url))
                        #goal_diff = parse_int_from_xpath_text(row, "./td[9]")
                        points = parse_int_from_xpath_text(row, "./td[10]")
                    else:
                        win_count = 0
                        draw_count = 0
                        lost_count = 0
                        goals_for = None,
                        goals_against = None,
                        points = 0

                    group_standing.append({
                        'team': team_display_name,
                        'rank': rank,
                        'played': match_count,
                        'won': win_count,
                        'drawn': draw_count,
                        'lost': lost_count,
                        'goal_for': goals_for,
                        'goal_against': goals_against,
                        'points': points
                    })
                    noncompetitive = False
                    withdrawn = False
                else:
                    group_team_comment = row.xpath("./td[5]/text()").get().strip()
                    if group_team_comment == 'außer Konkurrenz':
                        group_standing.append({
                            'team': team_display_name,
                            'rank': rank,
                            'noncompetitive': True,
                            'withdrawn': False,
                        })
                        noncompetitive = True
                        withdrawn = False
                    elif group_team_comment.startswith("zurückgezogen"):
                        group_standing.append({
                            'team': team_display_name,
                            'rank': rank,
                            'noncompetitive': False,
                            'withdrawn': True,
                        })
                        noncompetitive = True
                        withdrawn = True
                    else:
                        self.logger.warn("Uknown comment in team %s for group %s: \"%s\" URL: %s" % (team_display_name, group_name, group_team_comment, response.url))
                        group_standing.append({
                            'team': team_display_name,
                            'rank': rank,
                            'noncompetitive': False,
                            'withdrawn': True,
                        })
                        noncompetitive = True
                        withdrawn = True

                team_portrait_url = response.urljoin(team_link.attrib.get('href'))
                team_id = get_team_id_from_url(team_portrait_url)
                group_team_ids.append(team_id)
                yield scrapy.Request(team_portrait_url, callback=self.parse_team_portrait_page,
                                     cb_kwargs={'display_name': team_display_name, 'noncompetitive': noncompetitive,
                                                'withdrawn': withdrawn})
            break
        yield items.ChampionshipGroup(nuliga_id=group_id, name=group_name,
                                      championship=championship_short_name,
                                      report_pdf_url=group_report_pdf, team_nuliga_ids=group_team_ids,
                                      standings=group_standing)

        spielplan_url = response.urljoin(response.xpath("//li/a[text()='Spielplan (Gesamt)']/@href").get())
        yield scrapy.Request(spielplan_url, callback=self.parse_group_meetings_page)

    def parse_group_meetings_page(self, response: scrapy.http.Response):
        """"
        Example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/groupPage?displayTyp=vorrunde&displayDetail=meetings&championship=%C3%96HB+22%2F23&group=211667
        """
        title = response.xpath("//h1/text()").getall()
        # expected: first line is championship, second line is group name, third is "Tabelle und Spielplan (Aktuell)"
        if len(title) < 3:
            # unexpected title format
            self.logger.warn("unexpected <h1> format in %s" % response.url)
            return

        championship_name = title[0].strip()
        group_name = title[1].strip()
        if "Spielplan (gesamt)" != title[2].strip():
            self.logger.error("Wrong title in parse_group_meetings_page: %s" % title[2].strip())
            return

        tables = response.xpath("//table[has-class('result-set')]")
        for table in tables:
            if "Heimmannschaft" != table.xpath(".//th[4]/text()").get()\
                    or "Gastmannschaft" != table.xpath(".//th[5]/text()").get():
                # not match table
                continue
            rows = table.xpath(".//tr")
            match_date = None
            for row in rows:
                if row.xpath("./th"):
                    # skip header row
                    continue
                match_day = row.xpath("./td[1]/text()").get().strip()
                if match_day in ["nicht terminiert", "Termin offen"]:
                    # matches without any date set
                    match_date = None
                    match_time = None
                    court_td_index = None
                    match_num_index = 4
                    team_a_index = 5
                    team_b_index = 6
                    match_result_index = 7
                elif match_day == "":
                    # no date set, use date from previous row
                    match_time = row.xpath("./td[3]/text()").get().strip()
                    court_td_index = 4
                    match_num_index = 5
                    team_a_index = 6
                    team_b_index = 7
                    match_result_index = 8
                else:
                    match_date = row.xpath("./td[2]/text()").get().strip()
                    match_time = row.xpath("./td[3]/text()").get().strip()
                    court_td_index = 4
                    match_num_index = 5
                    team_a_index = 6
                    team_b_index = 7
                    match_result_index = 8

                if court_td_index and row.xpath("./td[%d]/span" % court_td_index):
                    court_name = row.xpath("./td[%d]/span/@title" % court_td_index).get().strip()
                    court_short_name = row.xpath("./td[%d]//a/text()" % court_td_index).get().strip()
                    court_info_page_url = response.urljoin(row.xpath("./td[%d]//a/@href" % court_td_index).get().strip())
                    yield scrapy.Request(court_info_page_url, callback=self.parse_court_info_page)
                else:
                    court_name = None

                if match_num_index:
                    match_num = row.xpath("./td[%d]/text()" % match_num_index).get().strip()
                else:
                    match_num = None

                team_a_name = row.xpath("./td[%d]/text()" % team_a_index).get().strip()
                team_b_name = row.xpath("./td[%d]/text()" % team_b_index).get().strip()
                if team_a_name.endswith(" (a.K.)"):  # "außer Konkurenz"-indicator may be found in display name
                    team_a_name = team_a_name[:-7]
                if team_b_name.endswith(" (a.K.)"):  # "außer Konkurenz"-indicator may be found in display name
                    team_b_name = team_b_name[:-7]
                if row.xpath("./td[%d]/span/a/span" % match_result_index):
                    match_report_url = row.xpath("./td[8]/span/a/@href").get()
                    match_id = get_match_id_from_url(match_report_url)
                    match_result = row.xpath("./td[%d]/span/a/span/text()" % match_result_index).get().strip()
                    (goals_a, goals_b) = match_result.split(':', 2)
                    goals_a = int(goals_a)
                    goals_b = int(goals_b)
                    match_detail_page_url = response.urljoin(row.xpath("./td[%d]/span/a/@href" % match_result_index).get().strip())
                else:
                    goals_a = goals_b = None
                    match_id = None

                match_flag = None
                if match_date and match_time:
                    if len(match_time) != 5:
                        # parse date flags
                        # t: date changed
                        # v: home team rights switched
                        # h: court changed
                        match_flag = match_time[5:].strip()
                        if match_flag not in ['t', 'v', 'h']:
                            self.logger.error("UNKNOWN MATCH FLAG: %s" % match_flag)
                        match_time = match_time[:5]
                    match_datetime = datetime.strptime("%s - %s" % (match_date, match_time), "%d.%m.%Y - %H:%M")
                else:
                    match_datetime = None

                match = items.Match(
                    nuliga_id=match_id,
                    championship=championship_name,
                    group=group_name,
                    match_number=match_num,
                    team_a=team_a_name,
                    team_b=team_b_name,
                    goals_a=goals_a,
                    goals_b=goals_b,
                    date=match_datetime,
                    flag=match_flag,
                    court=court_name
                )
                yield match

    def parse_team_portrait_page(self, response: scrapy.http.Response, display_name=None, noncompetitive=False,
                                 withdrawn=False):
        """
        Example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/teamPortrait?teamtable=1503481&pageState=vorrunde&championship=WHV+22%2F23&group=211872
        """
        title = response.xpath("//h1/text()").getall()
        # expected: first line is championship, second line is group name, third is team name
        if len(title) < 3:
            # unexpected title format
            self.logger.warn("unexpected <h1> format in %s" % response.url)
            return

        response_url_parts = urlsplit(response.url)
        base_url = f'{response_url_parts.scheme}://{response_url_parts.netloc}'

        championship_name = title[0].strip()
        championship_short_name = get_championship_short_name_from_url(response.url)
        group_id = get_group_id_from_url(response.url)
        group_name = title[1].strip()
        team_portrait_name = title[2].strip()
        team_id = get_team_id_from_url(response.url)

        club_name = None
        club_id = None
        squad = set()
        tables = response.xpath("//table[has-class('result-set')]")
        for table in tables:
            if len(table.xpath(".//tr/th")) == 0 and len(table.xpath('.//tr[1]/td')) == 2:
                # this is the team info table (first table on page)
                rows = table.xpath(".//tr")
                for row in rows:
                    if 'Verein' == row.xpath("./td[1]/b/text()").get():
                        club_name = row.xpath("./td[2]/a[1]/text()").get()
                        club_info_url = response.urljoin(row.xpath("./td[2]/a[1]/@href").get())
                        club_id = get_club_id_from_url(club_info_url)
                        yield scrapy.Request(club_info_url, callback=self.parse_club_info)
            if table.xpath(".//tr/th[1][text()='Tag Datum Zeit']"):
                # this is the schedule table (second table on page)
                continue
            if table.xpath(".//tr/th[1][text()='Name, Vorname']"):
                # this is the squad table (third table on page)
                rows = table.xpath(".//tr")
                for row in rows:
                    if len(row.xpath('./td')) < 2:
                        # header row (containing 1 <td> or only <th> elements)
                        continue
                    player_name = row.xpath("./td[1]/a[1]/text()").get()
                    if not player_name:
                        continue
                    player_name = player_name.strip()
                    player_page_url = row.xpath("./td[1]/a[1]/@href").get()
                    if player_page_url.startswith('/'):
                        # seems like player pages are only linked by their absolute paths, not absolute URLs. Fix this:
                        player_page_url = f'{base_url}{player_page_url}'
                    player_id = get_player_id_from_url(player_page_url)
                    if len(row.xpath('./td')) == 4:
                        # default table layout data-row
                        # Columns "Name, Vorname", "Position", "Trikotnummer", "Status"
                        position_td_num = 2
                        squad_num_td_num = 3
                    if len(row.xpath('./td')) == 5:
                        # wide table layout - seen when second column "SG-Verein" is present
                        # example: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/teamPortrait?teamtable=1515649&pageState=vorrunde&championship=%C3%96HB+24%2F25&group=214862
                        position_td_num = 3
                        squad_num_td_num = 4
                    position = row.xpath(f"./td[{position_td_num}]/text()").get()
                    position = position.strip() if position else ''
                    squad_number = row.xpath(f"./td[{squad_num_td_num}]/text()").get()
                    squad_number =squad_number.strip() if squad_number else ''
                    team_squad_item = items.TeamSquadItem(nuliga_id=player_id, name=player_name, squad_number=squad_number, position=position)
                    squad.add(team_squad_item)
                    if position and position != 'Spieler' and position != 'Torwart':
                        self.logger.info(f'Squad player: {player_name} has unexpected position: {position}: {player_page_url}')

        if not club_name or not club_id:
            self.logger.error('Incomplete team profile: no club information detected - %s' % response.url)
            return

        team_name = team_portrait_name[len(club_name):].strip().replace("\xa0", " ")

        yield items.Team(nuliga_id=team_id, name=team_name, display_name=display_name, club_name=club_name, championship=championship_short_name,
                         group=group_name, noncompetitive=noncompetitive, withdrawn=withdrawn, squad=squad)

    def parse_court_info_page(self, response: scrapy.http.Response):
        """
        Example URL: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/courtInfo?federation=WHV&location=18368
        """
        title = response.xpath("//h1/text()").getall()
        # expected: first line is court name, second line is "Hallenspielplan"
        if len(title) < 2:
            # unexpected title format
            self.logger.warn("unexpected <h1> format in %s" % response.url)
            return

        court_title = title[0].strip()
        title_match = re.fullmatch(r"^(.+) \((.+)\)$", court_title)
        if not title_match:
            self.logger.warn("Unexpected court title: %s" % court_title)
            return

        court_id = get_court_id_from_url(response.url)

        court_name = title_match.group(1)
        court_short_name = title_match.group(2)

        address_lines = response.xpath("//h2[text()='Hallenadresse']//following::p[1]/text()").getall()
        if address_lines[0].strip() == '-':
            # no address
            address = None
            postal_code = None
            city = None
        elif len(address_lines) >= 2:
            for address_line in address_lines:
                address_line.strip()
            address = address_lines[0].strip()
            (postal_code, city) = address_lines[1].strip().split("\n", 2)
            postal_code = postal_code.strip()
            city = city.strip()
        else:
            address = None
            postal_code = None
            city = None
            self.logger.warn("INVALID ADDRESS: %s" % ";".join(address_lines))

        court = items.Court(
            nuliga_id=court_id,
            name=court_name,
            short_name=court_short_name,
            address=address,
            postal_code=postal_code,
            city=city
        )
        yield court

