
from parsel.selector import SelectorList


def parse_str_from_xpath_text(selector: SelectorList, xpath: str) -> str | None:
    """
    Resolve a xpath query on a selector to a str.
    If not resolvable return None.
    :param selector:
    :param xpath:
    :return:
    """
    result_text = selector.xpath("%s/text()" % xpath).get()
    if not result_text:
        return None
    result_text = result_text.strip()
    if not result_text:
        return None
    try:
        return str(result_text)
    except ValueError:
        return None


def parse_int_from_xpath_text(selector: SelectorList, xpath: str) -> int | None:
    """
    Resolve a xpath query on a selector to an int.
    If not resolvable return None.
    :param selector:
    :param xpath:
    :return:
    """
    result_text = parse_str_from_xpath_text(selector, xpath)
    if not result_text:
        return None
    try:
        return int(result_text)
    except ValueError:
        return None
