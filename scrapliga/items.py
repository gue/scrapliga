# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import time
import scrapy

# define a constant timestamp for this run of scrapy
_item_timestamp = int(time.time())


class BaseItem(scrapy.Item):
    timestamp = scrapy.Field()
    nuliga_id = scrapy.Field()

    def __init__(self, *args, **kwargs):
        kwargs['timestamp'] = _item_timestamp
        scrapy.Item.__init__(self, *args, **kwargs)


class Championship(BaseItem):
    name = scrapy.Field()
    short_name = scrapy.Field()


class ChampionshipGroup(BaseItem):
    name = scrapy.Field()
    championship = scrapy.Field()
    report_pdf_url = scrapy.Field()
    team_nuliga_ids = scrapy.Field()
    standings = scrapy.Field()


class Club(BaseItem):
    name = scrapy.Field()
    website = scrapy.Field()


class Team(BaseItem):
    name = scrapy.Field()
    display_name = scrapy.Field()
    club_name = scrapy.Field()
    championship = scrapy.Field()
    group = scrapy.Field()
    noncompetitive = scrapy.Field()
    withdrawn = scrapy.Field()
    squad = scrapy.Field()

class TeamSquadItem(BaseItem):
    name = scrapy.Field()
    squad_number = scrapy.Field()


class Court(BaseItem):
    name = scrapy.Field()
    short_name = scrapy.Field()
    address = scrapy.Field()
    postal_code = scrapy.Field()
    city = scrapy.Field()


class Match(BaseItem):
    championship = scrapy.Field()
    group = scrapy.Field()
    match_number = scrapy.Field()
    team_a = scrapy.Field()
    team_b = scrapy.Field()
    goals_a = scrapy.Field()
    goals_b = scrapy.Field()
    date = scrapy.Field()
    flag = scrapy.Field()
    court = scrapy.Field()

